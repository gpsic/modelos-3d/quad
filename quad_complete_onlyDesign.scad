fn=1024;

module helice(){
    translate([75,50,0])difference(){
        cylinder(d=200,h=1);
        translate([-75,-650,-1])cube(size=[1000,1300,5]);
    }
    rotate([0,0,180])    translate([75,50,0])difference(){
        cylinder(d=200,h=1);
        translate([-75,-650,-1])cube(size=[1000,1300,5]);
    }
    cylinder(d=6,h=8);
}
//helice();
color([0.0, 0.2, 0.2]){
    translate([0,0,2])rotate([0,180,0])import("quad_bott_plate.stl", convexity=3);
    translate([0,0,37])rotate([0,180,0])import("quad_top_plate.stl", convexity=3);
}
color([0.9, 0.2, 0.2]){
    rotate([180,0,45])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
    rotate([180,0,-45])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
}
color([1.0, 1.0, 0.9]){
    rotate([180,0,135])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
    rotate([180,0,-135])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
    rotate([0,90,45])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
    rotate([0,90,135])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
    rotate([0,90,-45])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
    rotate([0,90,-135])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
}
color([0.0, 0.2, 0.3]){
    translate([180,180,35])cylinder(d=26,h=30);
    translate([-180,180,35])cylinder(d=26,h=30);
    translate([180,-180,35])cylinder(d=26,h=30);
    translate([-180,-180,35])cylinder(d=26,h=30);
}
color([0,0,0,0.8]){
    translate([180,180,35+30])helice();
    translate([-180,180,35+30])helice();
    translate([180,-180,35+30])helice();
    translate([-180,-180,35+30])helice();
}
color([0.1,0.5,0.8])translate([0,0,17.5])cube(size=[160,40,30],center=true);