$fn=256;

pillar_d=13;
pillar_h=4;
hole_arm_d=3.5;
hole_arm_dist=25;

difference(){
union(){
    translate([0,hole_arm_dist/2,0])cylinder(d=pillar_d,h=pillar_h);
translate([0,-hole_arm_dist/2,0])cylinder(d=pillar_d,h=pillar_h);
    translate([-pillar_d/2,-hole_arm_dist/2,0])cube([pillar_d,hole_arm_dist,pillar_h]);
}
translate([0,hole_arm_dist/2,-1])cylinder(d=hole_arm_d,h=pillar_h+2);
translate([0,-hole_arm_dist/2,-1])cylinder(d=hole_arm_d,h=pillar_h+2);
}