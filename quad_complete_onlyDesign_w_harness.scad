fn=1024;

module helice(){
    translate([75,50,0])difference(){
        cylinder(d=200,h=1);
        translate([-75,-650,-1])cube(size=[1000,1300,5]);
    }
    rotate([0,0,180])    translate([75,50,0])difference(){
        cylinder(d=200,h=1);
        translate([-75,-650,-1])cube(size=[1000,1300,5]);
    }
    cylinder(d=6,h=8);
}
//helice();
color([0.0, 0.2, 0.2]){
    translate([0,0,2])rotate([0,180,0])import("quad_bott_plate.stl", convexity=3);
    translate([0,0,37])rotate([0,180,0])import("quad_top_plate.stl", convexity=3);
}
color([0.9, 0.2, 0.2]){
    rotate([180,0,45])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
    rotate([180,0,-45])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
}
color([1.0, 1.0, 0.9]){
    rotate([180,0,135])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
    rotate([180,0,-135])translate([51.5,0,-35])import("quad_arm.stl", convexity=3);
    rotate([0,90,45])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
    rotate([0,90,135])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
    rotate([0,90,-45])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
    rotate([0,90,-135])translate([-5,0,-92])import("quad_landing.stl", convexity=3);
}
color([0.0, 0.2, 0.3]){
    translate([180,180,35])cylinder(d=26,h=30);
    translate([-180,180,35])cylinder(d=26,h=30);
    translate([180,-180,35])cylinder(d=26,h=30);
    translate([-180,-180,35])cylinder(d=26,h=30);
}
color([0,0,0,0.8]){
    translate([180,180,35+30])helice();
    translate([-180,180,35+30])helice();
    translate([180,-180,35+30])helice();
    translate([-180,-180,35+30])helice();
}
color([0.1,0.5,0.8])translate([0,0,17.5])cube(size=[160,40,30],center=true);

$fn = 100;
module link(R=20, espesor=2, r=10) {
    rotate_extrude()
    translate([R, 0, 0])
    difference() {
        circle(r = r/2);
        circle(r = (r-espesor)/2);
    }
}
module base(R=20, espesor=2, r=10) {
    module joint(R=R, espesor=espesor, r=r) {
        difference() {
            cylinder(r=r/2+espesor, h=espesor);
            translate([0, 0, -espesor/2])cylinder(r=r/2-espesor/2, h=2*espesor);
            translate([0, 0, espesor])rotate_extrude()translate([r/2+espesor, 0, 0])circle(r = espesor);
        }
    }
    rotate([0, -90, 0])translate([espesor, 0, 0])union() {
        rotate([0, 90, 0])translate([0, R, 0])joint();
        rotate([0, 90, 0])translate([0, -R, 0])joint();
        difference() {
            translate([espesor, 0, 0])link(R, espesor, r);
            translate([-3*R+espesor, -1.5*R, -1.5*R])cube(3*R );
        }
        translate([-espesor, -(R+espesor+r/2), -espesor-r/2])cube([espesor, 2*(R+espesor+r/2), 2*espesor+r]);
    }
}

module harness() {
color([0.0, 0.2, 0.2]){base();}
color([0.9, 0.2, 0.2]){translate([0, -15, 26])rotate([0, 45, 45])link();}
color([0.0, 0.2, 0.2]){translate([-8, -15, 45])rotate([45, 0, 45])link();}
color([0.9, 0.2, 0.2]){translate([-25, -23, 55])rotate([0, 17, 22])link();}
}

rotate([0, 180, 0])harness();