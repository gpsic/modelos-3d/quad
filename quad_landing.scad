$fn=256;

use <quad_bott_plate.scad>

espesor=5;

module triang(){
    //cube(size = [14,33,2.5]);
    polyhedron
    (points = [
	       [0,0,0], [0,5,0], [0,5,10], [0,0,10], [14,0,0], [14,5,0]
    ], 
     faces = [
		  [0,1,3],[1,2,3],[0,3,4],[5,2,1],[2,4,3],[2,5,4],[0,4,1],[1,4,5]
		  ]
     );
}

module soporte(){
    translate([-11,0,5]){difference(){
        rotate([0,90,0])translate([0,-20,15])cube(size=[espesor,40,55]);
        translate([25,-15,-10])difference(){
            cube(size=[40,12.5,20]);
            translate([40,-65,0])cylinder(d=150,h=30);
        }
        translate([65,-80,-10])cylinder(d=145,h=30);
        mirror([0,1,0])translate([25,-15,-10])difference(){
            cube(size=[40,12.5,20]);
            translate([40,-65,0])cylinder(d=150,h=30);
        }
        mirror([0,1,0])translate([65,-80,-10])cylinder(d=145,h=30);
    }
    translate([20,0,-espesor])rotate([0,0,-45])cube(size=[3,20,espesor]);
    translate([20,0,-espesor])rotate([0,0,-135])translate([-3,0,0])cube(size=[3,20,espesor]);
    translate([15,-20,-5])cube(size=[4,40,15]);
    translate([19,-2.5,0])triang();}
}

translate([0,0,3])rotate([0,90,0])difference(){
    translate([3,0,0])rotate([0,-90,0])soporte();
    translate([-5.75,-12.5,-5])cylinder(d=4,h=15);
    translate([-5.75,+12.5,-5])cylinder(d=4,h=15);
}
translate([59,0,0])difference(){
    cylinder(d=15.35,h=5);
    translate([-21,-10,-1])cube(size=[20,20,7]);
}