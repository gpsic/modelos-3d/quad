$fn=256;

//main block dimensions
main_x=150;
main_y=150;
main_z=3;
main_supp=2;    //espesor refuerzos
main_supp_d=20; //distancia x e y al centro
aux=main_supp_d*1.41*2;

//arm holes (distance holes front, distance holes back, distances holes front to back, hole diameter, distance to plate center)
arm_dhf=25;
arm_dhb=25;
arm_dhfb=25;
arm_hdiam=3.3;
arm_dcent=50;

//battery strap (hole width and length, dist between hole centers)
batt_w=5;
batt_l=20;
batt_d=45;


module bloque_ppal(xdim ,ydim ,zdim){
    translate([-xdim/2,-ydim/2,0])cube(size = [xdim,ydim,zdim]);
    
    translate([-xdim/2,-main_supp/2,main_z])cube(size = [xdim,main_supp,main_supp]);
    //translate([-main_supp/2,-ydim/2,main_z])cube(size = [main_supp,ydim,main_supp]);
    
    translate([main_supp_d,main_supp_d,0])rotate([0,0,45])translate([-main_supp/2,-ydim/2,main_z])cube(size = [main_supp,ydim,main_supp]);
    translate([main_supp_d,-main_supp_d,0])rotate([0,0,135])translate([-main_supp/2,-ydim/2,main_z])cube(size = [main_supp,ydim,main_supp]);
    translate([-main_supp_d,-main_supp_d,0])rotate([0,0,45])translate([-main_supp/2,-ydim/2,main_z])cube(size = [main_supp,ydim,main_supp]);
    translate([-main_supp_d,main_supp_d,,0])rotate([0,0,135])translate([-main_supp/2,-ydim/2,main_z])cube(size = [main_supp,ydim,main_supp]);
    rotate([0,0,45])translate([-main_supp/2,-aux/2,main_z])cube(size = [main_supp,aux,main_supp]);
    rotate([0,0,135])translate([-main_supp/2,-aux/2,main_z])cube(size = [main_supp,aux,main_supp]);
}
module brazo_hueco_top(){
    
    translate([arm_dhfb/2,arm_dhf/2,-1])cylinder(h=main_z+2,d=arm_hdiam);
    translate([arm_dhfb/2,-arm_dhf/2,-1])cylinder(h=main_z+2,d=arm_hdiam);
    translate([-arm_dhfb/2,arm_dhb/2,-1])cylinder(h=main_z+2,d=arm_hdiam);
    translate([-arm_dhfb/2,-arm_dhb/2,-1])cylinder(h=main_z+2,d=arm_hdiam);
    translate([arm_dhfb/2+arm_hdiam/2+4,-main_y/4,-1])cube(size = [main_x/2,main_y/2,main_z+2]);
}

module brazo_hueco_bottom(){
    
    translate([arm_dhfb/2,arm_dhf/2,-1])cylinder(h=main_z+2,d=arm_hdiam);
    translate([arm_dhfb/2,-arm_dhf/2,-1])cylinder(h=main_z+2,d=arm_hdiam);
    translate([arm_dhfb/2+arm_hdiam/2+4,-main_y/4,-1])cube(size = [main_x/2,main_y/2,main_z+2]);
}


module quad_bottom(){
difference(){
    bloque_ppal(main_x,main_y,main_z);
    translate([arm_dcent,arm_dcent,0])rotate([0,0,45])brazo_hueco_bottom();
    translate([-arm_dcent,arm_dcent,0])rotate([0,0,135])brazo_hueco_bottom();
    translate([arm_dcent,-arm_dcent,0])rotate([0,0,-45])brazo_hueco_bottom();
    translate([-arm_dcent,-arm_dcent,0])rotate([0,0,-135])brazo_hueco_bottom();
    translate([130.3,0,-1])cylinder(h=main_z+10,d=main_x);
    translate([-130.3,0,-1])cylinder(h=main_z+10,d=main_x);
    translate([0,130.3,-1])cylinder(h=main_z+10,d=main_x);
    translate([0,-130.3,-1])cylinder(h=main_z+10,d=main_x);
    
    //battery strap
    translate([-batt_l/2,-batt_w/2+batt_d/2,-1])cube(size = [batt_l,batt_w,10]);
    translate([-batt_l/2,-batt_w/2-batt_d/2,-1])cube(size = [batt_l,batt_w,10]);
    translate([-batt_l/2+batt_l*1.6,-batt_w/2+batt_d/2,-1])cube(size = [batt_l,batt_w,10]);
    translate([-batt_l/2+batt_l*1.6,-batt_w/2-batt_d/2,-1])cube(size = [batt_l,batt_w,10]);
    translate([-batt_l/2-batt_l*1.6,-batt_w/2+batt_d/2,-1])cube(size = [batt_l,batt_w,10]);
    translate([-batt_l/2-batt_l*1.6,-batt_w/2-batt_d/2,-1])cube(size = [batt_l,batt_w,10]);
}
}
quad_bottom();