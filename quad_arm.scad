$fn=256;

//main block
sup_x=38;
sup_y=38;
sup_z=7;

arm_x=184;
arm_y=38;
arm_z=10;

mot_d=38;
mot_z=12;
mot_h=5;
mot_w=3;

pillar_d=13;
pillar_h=33;
hole_arm_d=3.5;
hole_arm_dist=25;

module cross_hole(){
   translate([0,0,arm_z/2])cube(size=[hole_arm_dist,hole_arm_dist/2.5,arm_z+2],center=true);
   rotate([0,0,90])translate([0,0,arm_z/2])cube(size=[hole_arm_dist,hole_arm_dist/2.5,arm_z+2],center=true); 
}
module semicirc_hole(){
    difference(){
        cylinder(d=mot_d*2/3,h=mot_z);
        translate([0,0,-1])cylinder(d=mot_d*1/3,h=mot_z+2);
        translate([0,0,-1])rotate([0,0,60])cube(size=[mot_d,mot_d/2,mot_z+2]);
        translate([0,0,-1])rotate([0,0,-60])cube(size=[mot_d,mot_d/2,mot_z+2]);
        translate([0,0,-1])rotate([0,0,-140])cube(size=[mot_d,mot_d/2,mot_z+2]);
        translate([0,0,-1])rotate([0,0,-215])cube(size=[mot_d,mot_d/2,mot_z+2]);
    }
}
module motor_sup(){
    difference(){
        cylinder(d=mot_d,h=mot_z);
        translate([0,0,mot_h])cylinder(d=mot_d-2*mot_w,h=mot_z);
        translate([9.5,0,-1])cylinder(d=hole_arm_d,h=mot_h+2);
        translate([-9.5,0,-1])cylinder(d=hole_arm_d,h=mot_h+2);
        translate([0,8,-1])cylinder(d=hole_arm_d,h=mot_h+2);
        translate([0,-8,-1])cylinder(d=hole_arm_d,h=mot_h+2);
        translate([0,0,-1])cylinder(d=9,h=mot_h+2);
//        translate([0,0,-1])semicirc_hole();
//        rotate([0,0,90])translate([0,0,-1])semicirc_hole();
//        rotate([0,0,180])translate([0,0,-1])semicirc_hole();
//        rotate([0,0,-90])translate([0,0,-1])semicirc_hole();
    }
    
}
module sup_tri(){
    polyhedron
    (points = [
	       [0,0,0], [0,pillar_d/2,0], [0,pillar_d/2,pillar_h], [0,0,pillar_h], [pillar_h/2,0,0], [pillar_h/2,pillar_d/2,0]
    ], 
     faces = [
		  [0,1,3],[1,2,3],[0,3,4],[5,2,1],[2,4,3],[2,5,4],[0,4,1],[1,4,5]
		  ]
     );
}
module sup_col_tri(){
    //size=[arm_x-mot_d+mot_w,arm_y/4,pillar_h*2/3-mot_z]
    polyhedron
    (points = [
	       [0,0,0], [0,arm_y/4,0], [0,arm_y/4,(pillar_h*2/3-mot_z)*0.5], [0,0,(pillar_h*2/3-mot_z)*0.5], [arm_x-mot_d+mot_w,0,0], [arm_x-mot_d+mot_w,arm_y/4,0]
    ], 
     faces = [
		  [0,1,3],[1,2,3],[0,3,4],[5,2,1],[2,4,3],[2,5,4],[0,4,1],[1,4,5]
		  ]
     );
}
module sup_cross(){
    rotate([0,0,45])translate([-arm_x/2,-mot_w/2,0])cube(size=[arm_x,mot_w,arm_z*0.8]);
    rotate([0,0,-45])translate([-arm_x/2,-mot_w/2,0])cube(size=[arm_x,mot_w,arm_z*0.8]);
}
module sup_cross_quad(){
    translate([55,0,0])sup_cross();
    translate([90,0,0])sup_cross();
    translate([125,0,0])sup_cross();
    translate([160,0,0])sup_cross();
}
module sup_cross_full(){
    difference(){
        sup_cross_quad();
        translate([sup_x+(arm_x-mot_d)/2,-b,-1])cylinder(d=a-9,h=pillar_h+2);
        translate([sup_x+(arm_x-mot_d)/2,+b,-1])cylinder(d=a-9,h=pillar_h+2);
        
    }
}
module main_block(){
    translate([0,-sup_y/2,0])cube(size=[sup_x,sup_y,sup_z]);
    difference(){
        translate([sup_x/2,-arm_y/2,0])cube(size=[arm_x,arm_y,arm_z]);
        translate([sup_x/2+arm_x,0,-1])cylinder(d=mot_d-0.05,h=mot_z+2);;
    }
    translate([sup_x/2+arm_x,0,0])motor_sup();
    //pillars
    translate([sup_x/2+hole_arm_dist/2,hole_arm_dist/2,0])cylinder(d=pillar_d,h=pillar_h);
    translate([sup_x/2+hole_arm_dist/2,-hole_arm_dist/2,0])cylinder(d=pillar_d,h=pillar_h);
    translate([sup_x/2+hole_arm_dist/2,hole_arm_dist/2,0])cube(size=[pillar_d/2,pillar_d/2,pillar_h]);
    translate([sup_x/2+hole_arm_dist/2,-pillar_d/4-hole_arm_dist/2-pillar_d/4,0])cube(size=[pillar_d/2,pillar_d/2,pillar_h]);
    translate([sup_x/2+hole_arm_dist/2,-hole_arm_dist/2,0])cube(size=[pillar_d/2,hole_arm_dist,pillar_h*2/3]);
    translate([sup_x,-sup_y/2,0])sup_tri();
    translate([sup_x,sup_y/2-pillar_d/2,0])sup_tri();
    //arm column
    translate([sup_x,-arm_y/8,0])cube(size=[arm_x-mot_d+mot_w,arm_y/4,mot_z]);
    translate([sup_x,-arm_y/8,mot_z])sup_col_tri();
    //arm support
}
a=1500;
b=760;
module arm(){
difference(){
    main_block();
    //holes
    translate([sup_x/2-hole_arm_dist/2,hole_arm_dist/2,-1])cylinder(d=hole_arm_d,h=sup_z+2);
    translate([sup_x/2-hole_arm_dist/2,-hole_arm_dist/2,-1])cylinder(d=hole_arm_d,h=sup_z+2);
    translate([sup_x/2+hole_arm_dist/2,hole_arm_dist/2,-1])cylinder(d=hole_arm_d,h=pillar_h+2);
    translate([sup_x/2+hole_arm_dist/2,-hole_arm_dist/2,-1])cylinder(d=hole_arm_d,h=pillar_h+2);
    translate([sup_x/2,0,0])cross_hole();
    //arm empty zones
    difference(){
            translate([sup_x,-arm_y*3/8,-1])cube(size=[arm_x-mot_d,arm_y/4,pillar_h+2]);
        translate([sup_x+(arm_x-mot_d)/2,-b,-1])cylinder(d=a-3,h=pillar_h+2);
    }
    translate([sup_x+(arm_x-mot_d)/2,-b,-1])cylinder(d=a-9,h=pillar_h+2);
    
    difference(){
        translate([sup_x,+arm_y*1/8,-1])cube(size=[arm_x-mot_d,arm_y/4,pillar_h+2]);
        translate([sup_x+(arm_x-mot_d)/2,+b,-1])cylinder(d=a-3,h=pillar_h+2);
    }
    translate([sup_x+(arm_x-mot_d)/2,+b,-1])cylinder(d=a-9,h=pillar_h+2);
}
//arm support
sup_cross_full();
}
arm();